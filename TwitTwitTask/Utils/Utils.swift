//
//  Utils.swift
//  Tiny Colorbook
//
//  Created by Roman Kovalchuk on 2/10/15.
//  Copyright (c) 2015 tap2Fly. All rights reserved.
//

import UIKit
import SpriteKit
import SystemConfiguration

class Utils{
    
    let iPad: [CGFloat]! = [768] //ipad height
    let iPhone: [CGFloat]! = [736, 667, 568, 480] //iphon width for all devices
    
    class var inst : Utils {
        struct Static
        {
            static let instance : Utils = Utils()
        }
        return Static.instance
    }
    
    //MARK:- ****** METHODS ******
    
    func isiPhone() -> Bool{
        
        if find(iPhone, UIScreen.mainScreen().bounds.height) != nil {
            return true
        }else{
            return false
        }
    }
    
    func getIphone() -> String{
        switch(UIScreen.mainScreen().bounds.height){
        case iPhone[0]: return "iPhone4s"
        case iPhone[1]: return "iPhone5s"
        case iPhone[2]: return "iPhone6"
        case iPhone[3]: return "iPhone6p"
        default: return ""
        }
    }
    
    func loadImageFromUrl(urlString: String, imageView: UIImageView){
        if(urlString != ""){
            var imgURL: NSURL = NSURL(string: urlString)!
            let request: NSURLRequest = NSURLRequest(URL: imgURL)
            NSURLConnection.sendAsynchronousRequest(
                request, queue: NSOperationQueue.mainQueue(),
                completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
                    if error == nil {
                        imageView.image = UIImage(data: data)
                    }
            })
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0)).takeRetainedValue()
        }
        
        var flags: SCNetworkReachabilityFlags = 0
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == 0 {
            return false
        }
        
        let isReachable = (flags & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return isReachable && !needsConnection
    }
    
}

