//
//  CDUser.swift
//  TwitTwitTask
//
//  Created by Roman Kovalchuk on 9/4/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation
import CoreData

@objc(CDUser)
class CDUser: NSManagedObject {

    @NSManaged var date: NSNumber
    @NSManaged var imageUrl: String
    @NSManaged var message: String
    @NSManaged var name: String

}
