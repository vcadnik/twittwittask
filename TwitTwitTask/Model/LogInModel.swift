//
//  LogInModel.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import UIKit

class LogInModel{
    let controller: AuthViewController!
    
    // MARK: - Privacy
    private struct PKeys {
        let consumerKey = "3vjLx0zr0FqFLUVpdiAd3WOeJ"
        let secretKey = "A9WoFz4Y9AVPLzc8MW06TxZJwxXMTnXcZkXaofHnVzrc9XVtFi"
    }
    
    init(authViewController: AuthViewController){
        controller = authViewController
    }
    
    func signIn(){
        let loginController = twitter.loginControllerWithCompletionHandler { (Bool success) -> Void in
            println("success: \(success) \n")
        }
        controller.presentViewController(loginController, animated: true, completion: nil)
        
        twitter.permanentlySetConsumerKey(PKeys().consumerKey, andSecret: PKeys().secretKey)
        twitter.delegate = controller
        twitter.loadAccessToken()
        
    }
    
    func requestToken(){
        twitter.permanentlySetConsumerKey(PKeys().consumerKey, andSecret: PKeys().secretKey)
        twitter.delegate = controller
        twitter.loadAccessToken()
    }
 
}