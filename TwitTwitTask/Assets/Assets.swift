//
//  Assets.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation



let a_twitter = "Twitter"
let a_ok = "ok"
let a_msg_right = "Message was sended!"
let a_msg_wrong = "Sorry, something going wrong..."
let a_hi = "Hi, "
let a_data_loading = ", wait please, \n Twits are loading..."
let a_saccessHTTPbody = "SavedAccessHTTPBody"
let a_cell = "cell_ID"

let a_ids = "ids"
let a_undefined = "undefined"

let a_check_inet_connection = "check internet connection!!"

let a_message_not_found = "Message Not Found"
let a_name_is_not_found = "Name is Not Found"

//Credentials
let a_cr_array: [String] = [ "name", "text", "profile_image_url", "date"]

let a_singleTwitControllerId = "SingleTwitViewController"
let a_authControllerId = "AuthViewController"