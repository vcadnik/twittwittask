//
//  WriteATwitViewController.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/2/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import UIKit

class WriteATwitViewController: UIViewController{
    
    @IBOutlet weak var messageTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sendTwit(sender: AnyObject) {
        if let message: NSString = messageTextView.text {
            if(message.length > 0){
                if(Utils.inst.isConnectedToNetwork()){
                    let error =  FHSTwitterEngine.sharedEngine().postTweet(message as String)
                    if(error == nil){
                        AuthViewController.Message = a_msg_right
                    }else{
                        AuthViewController.Message = a_msg_wrong
                    }
                }else{
                    AuthViewController.Message = a_check_inet_connection
                }
            }
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
}
