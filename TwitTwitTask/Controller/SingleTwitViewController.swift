//
//  SingleTwitViewController.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import UIKit

class SingleTwitViewController: UIViewController{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var imageURL = ""
    var userName = "name"
    var message = "empty"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNameLabel.text = userName
        self.messageTextView.text = message
        
        utils.loadImageFromUrl(imageURL, imageView: imageView)

    }
    
    func inflate(user: User){
        self.imageURL = user.imageUrl
        self.userName = user.name
        self.message = user.message
    }
    
    @IBAction func backToTwitList(sender: AnyObject) {
        let authViewController: AuthViewController = self.storyboard!.instantiateViewControllerWithIdentifier(a_authControllerId) as! AuthViewController
        self.presentViewController(authViewController, animated: true, completion: nil)
    }
}