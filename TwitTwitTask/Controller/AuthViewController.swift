//
//  ViewController.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/2/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class AuthViewController: UIViewController, FHSTwitterEngineAccessTokenDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: - Static Variables
    static var IsFirstTimeLoad = true
    static var IsUserDataTimeLoad = false
    
    static var Message = ""
    
    // MARK: - View
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var twitContainer: UIView!
    @IBOutlet weak var addTwitButton: UIButton!
    @IBOutlet weak var twitsTableView: UITableView!
    
    // MARK: - Model
    private var logInModel: LogInModel!    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logInModel = LogInModel(authViewController: self)
        
        twitsTableView.delegate = self
        twitsTableView.dataSource = self
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        checkForTwitAuth()
    }
    
    @IBAction func logOut(sender: AnyObject) {
        twitter.clearAccessToken()
        checkForTwitAuth()
    }
    
    @IBAction func signIn(sender: AnyObject) {
        logInModel.signIn()
    }
    
    func checkForTwitAuth(){
        logInModel.requestToken()
        
        if(twitter.isAuthorized()){
            signInButton.hidden = true
            signInButton.enabled = false
            logOutButton.hidden = false
            logOutButton.enabled = true
            twitContainer.hidden = false
            addTwitButton.hidden = false
            addTwitButton.enabled = true
            twitsTableView.hidden = false
            
            DataCachingHelper().addCDUserToCoreData(twitsTableView)
            
            showAlert()
            
        }else{
            signInButton.hidden = false
            signInButton.enabled = true
            logOutButton.hidden = true
            logOutButton.enabled = false
            twitContainer.hidden = true
            addTwitButton.hidden = true
            addTwitButton.enabled = false
            twitsTableView.hidden = true
        }
    }
    
    // MARK: - Table View
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var size = 0
        if(twitter.isAuthorized()){
            size = DataCachingHelper.userDataDictionary.count
        }
        
        return size
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell:TwitTableViewCell!
        
        if(twitter.isAuthorized()){
            cell = tableView.dequeueReusableCellWithIdentifier(a_cell) as! TwitTableViewCell
            
            var arr = DataCachingHelper.userDataDictionary.values.array
            var sortedArr:[User] = sorted(arr){ $0.0.date.intValue > $1.date.intValue}
            cell!.inflateCell(sortedArr[indexPath.row])
            
        }
            
        else
            if(cell == nil){
                cell = tableView.dequeueReusableCellWithIdentifier(a_cell) as! TwitTableViewCell
                cell!.inflateCell(tParseHelper.parseUserFromTheLineUserByIds(a_undefined, isID: true, count: 1))
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let singleViewController: SingleTwitViewController = self.storyboard!.instantiateViewControllerWithIdentifier(a_singleTwitControllerId) as! SingleTwitViewController
        
        let cell = (tableView.cellForRowAtIndexPath(indexPath) as! TwitTableViewCell)
        singleViewController.inflate(User(imageUrl: cell.imageUrl, name: cell.userNameTextLabel.text!, message: cell.userMessageTextView.text!, date: cell.date))
        self.presentViewController(singleViewController, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let vrow = tableView.indexPathsForVisibleRows() {
            if(indexPath.row == vrow.last!.row) {
                //refresh when sliding from top
            }
        }
    }
    
    // MARK: - AlertView
    func makeAlertWithText(text: String){
        let alertView = UIAlertView(title: a_twitter, message: text, delegate: self, cancelButtonTitle: a_ok)
        alertView.alertViewStyle = UIAlertViewStyle(rawValue: 0)!
        alertView.show()
    }
    
    func showAlert(){
        if(AuthViewController.IsFirstTimeLoad && twitter.isAuthorized()){
            AuthViewController.IsFirstTimeLoad = false
            makeAlertWithText(a_hi+"\(tParseHelper.parseCredentialsByValue(a_cr_array[0]))!"
                + a_data_loading)
        }
        
        if(twitter.isAuthorized()){
            if(AuthViewController.Message != "") {
                makeAlertWithText(AuthViewController.Message)
                AuthViewController.Message = ""
            }
        }
    }
    
    // MARK: - FHSTwitterEngine
    func storeAccessToken(accessToken: String!) {
        NSUserDefaults.standardUserDefaults().setObject(accessToken, forKey: a_saccessHTTPbody)
    }
    
    func loadAccessToken() -> String! {
        return NSUserDefaults.standardUserDefaults().objectForKey(a_saccessHTTPbody) as? String
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
}