//
//  GlobalReference.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation

let twitter = FHSTwitterEngine.sharedEngine()
let tParseHelper = TwitterParseHelper.inst
let utils = Utils.inst
let cdh = CoreDataHelper.inst