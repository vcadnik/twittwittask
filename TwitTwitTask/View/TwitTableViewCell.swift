//
//  TwitTableCell.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import UIKit

class TwitTableViewCell: UITableViewCell{
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameTextLabel: UILabel!
    @IBOutlet weak var userMessageTextView: UITextView!
    
    var date: NSNumber = 0
    var imageUrl = ""
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func inflateCell(user: User){
        utils.loadImageFromUrl(user.imageUrl, imageView: userImageView)
        
        self.imageUrl = user.imageUrl
        self.userNameTextLabel.text = user.name
        self.userMessageTextView.text = user.message
    }
}
