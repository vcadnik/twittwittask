//
//  DataCachingHelper.swift
//  TwitTwitTask
//
//  Created by Roman Kovalchuk on 9/4/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation
import UIKit

class DataCachingHelper{
    
    static var userDataDictionary: [String: User] = [String: User]()
    let MAX_FETCHED_USERS_COUNT = 100 // users twits from core data(saved)
    let MAX_NEW_LOADED_USERS_COUNT = 100 //users twtis brand new(not saved)

    func checkForUndefinedUser(user: User) -> Bool {
        if( (user.message == a_message_not_found)){
            //Unrecognized User is Found!
            DataCachingHelper.userDataDictionary.removeValueForKey(user.message)
            return true
        }
        
        return false
    }
    
    func addCDUserToCoreData(twitsTableView: UITableView){

        //1) block user interaction for tableview until all data will be loaded
        dispatch_async(dispatch_get_main_queue(), {
            twitsTableView.userInteractionEnabled = false
        })
        
        //2) load from Core Data (no internet)
        var usersCountSaved = 0
        let cduser = cdh.fetchCoreData()
        if(cduser.count > 0){
            for e in cduser{
                if(usersCountSaved < MAX_FETCHED_USERS_COUNT){
                    let user = User(imageUrl: e.imageUrl, name: e.name, message: e.message, date: e.date)
                    DataCachingHelper.userDataDictionary[user.message] = user
                    usersCountSaved++
                    
                }
            }
        }
        println("Twits Fetched From Core Data(Old) Count: \(usersCountSaved)")

        
        if(Utils.inst.isConnectedToNetwork()){
            //3) load from the internet to dictionary
            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0), { ()->() in
                if(twitter.isAuthorized()){
                    self.loadSelfTwits(twitsTableView)
                    
                    if let friendsListIDs =  twitter.getFriendsIDs() as? NSDictionary {
                        if let idsArr = friendsListIDs.valueForKey(a_ids) as? NSArray {
                            self.loadFriendTwits(twitsTableView, idsArr: idsArr)
                        }
                    }
                }
                
                //make table view touches responsible after all data were loaded
                dispatch_async(dispatch_get_main_queue(), {
                    twitsTableView.userInteractionEnabled = true
                })
                
                //4) save to core data
                cdh.deleteAll()// TODO: WARNING - need to be edited!
                self.saveTwitsToCoreAfterLoad()
            })
        }
    }
    
    func loadSelfTwits(twitsTableView: UITableView){
        let user = tParseHelper.parseUserFromTheLineUserByIds(twitter.authenticatedID, isID: true, count: 1)
        if(!checkForUndefinedUser(user) && DataCachingHelper.userDataDictionary.indexForKey(user.message) == nil){
            DataCachingHelper.userDataDictionary[user.message] = user
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            twitsTableView.reloadData()
        })
    }
    
    func loadFriendTwits(twitsTableView: UITableView, idsArr: AnyObject){
        var usersCountNotSaved = 0
        for(var i=0; i<idsArr.count; i++){
            let userId = idsArr[i] as! String
            
            let user = tParseHelper.parseUserFromTheLineUserByIds(userId, isID: true, count: 1)
            if(!checkForUndefinedUser(user) && DataCachingHelper.userDataDictionary.indexForKey(user.message) == nil){
                if(usersCountNotSaved < MAX_NEW_LOADED_USERS_COUNT){
                   DataCachingHelper.userDataDictionary[user.message] = user
                   usersCountNotSaved++
                }
            }
            dispatch_async(dispatch_get_main_queue(), {
                twitsTableView.reloadData()
            })
        }
        
        println("Twits Loaded From Web (New) Count: \(usersCountNotSaved)")
    }
    
    
    func saveTwitsToCoreAfterLoad(){
        let arr = sorted((DataCachingHelper.userDataDictionary.values.array), {
            (e1: User, e2: User) -> Bool in
            return e1.date.intValue > e2.date.intValue
        })
        
        for(var i=0; i<arr.count; i++){
            cdh.insertCoreData(arr[i].name, message: arr[i].message, imageUrl: arr[i].imageUrl, date: arr[i].date)
        }
    }
}