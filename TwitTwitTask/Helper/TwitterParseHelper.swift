//
//  ParseHelper.swift
//  TestTaskTwit
//
//  Created by Roman Kovalchuk on 9/3/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation

class TwitterParseHelper{
    
    class var inst : TwitterParseHelper {
        struct Static
        {
            static let instance : TwitterParseHelper = TwitterParseHelper()
        }
        return Static.instance
    }
    
    func parseCredentialsByValue(value: String) -> NSString{
        if let result = twitter.verifyCredentials().valueForKey(value) as? NSString {
            return result
        }
        return ""
    }
    
    func parseTimeLineForUserByValue(value: String, user: String, isID: Bool, count: Int32) -> NSString{
        if let result = twitter.getTimelineForUser(user, isID: isID, count: count).valueForKey(value) as? NSString {
            return result
        }
        return ""
    }
    
    func parseUserFromTheLineUserByIds(userId: String, isID: Bool, count: Int32) ->User{
        var imageURl = ""
        var name = a_name_is_not_found
        var message = a_message_not_found
        var date = NSDate.timeIntervalSinceReferenceDate() * 1000
        
        if let array = twitter.getTimelineForUser(userId, isID: true, count: count) as? NSArray {
            
            if let array2 = array.valueForKey("user") as? NSArray {
                if let array3 = array2.valueForKey("name") as? NSArray {
                    if let tempName = array3[0] as? String {
                        name = tempName
                        
//                        println("user[name]: \(name)")
                    }

                }
                
                if let array4 = array2.valueForKey("profile_image_url") as? NSArray {
                    if let tempImgUrl = array4[0] as? String {
                        imageURl = tempImgUrl
//                        println("user[message]: \(imageURl)")
                    }
                }
            }
            
            if let array5 = array.valueForKey("text") as? NSArray{
                if let tempMsg = array5[0] as? String {
                    message = tempMsg
//                    println("user[message]: \(message)")
                }
            }
            
        }
        
        return User(imageUrl: imageURl, name: name, message: message, date: date)
    }
    
}