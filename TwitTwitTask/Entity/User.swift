//
//  User.swift
//  TwitTwitTask
//
//  Created by Roman Kovalchuk on 9/4/15.
//  Copyright (c) 2015 iceHead. All rights reserved.
//

import Foundation

class User{
    
    var date: NSNumber = 0
    var imageUrl: String = ""
    var message: String = a_message_not_found
    var name: String = a_name_is_not_found
    
    init(imageUrl: String, name: String, message: String, date: NSNumber){
        self.imageUrl = imageUrl
        self.name = name
        self.message = message
        self.date = date
    }
}